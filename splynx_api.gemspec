# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'splynx_api/version'

Gem::Specification.new do |spec|
  spec.name          = 'splynx_api'
  spec.version       = SplynxApi::VERSION
  spec.authors       = ['Splynx s.r.o. Team']
  spec.email         = ['buorn2011@gmail.com']

  spec.summary       = %q{Splynx API helper.}
  spec.description   = %q{Simple Splynx REST API helper.}
  spec.homepage      = 'https://bitbucket.org/splynx/splynx-ruby-api'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  # if spec.respond_to?(:metadata)
  #   spec.metadata['allowed_push_host'] = ''
  # else
  #   raise 'RubyGems 2.0 or newer is required to protect against ' \
  #     'public gem pushes.'
  # end

  spec.files                 = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir                = 'exe'
  spec.executables           = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths         = ['lib']
  spec.required_ruby_version = '>= 2.0.0'

  spec.add_development_dependency 'bundler', '~> 1.15'
  spec.add_development_dependency 'rake', '~> 10.0'

  spec.add_dependency 'rest-client', '~> 2.0.2'
  spec.add_dependency 'openssl', '~> 2.0.5'
  spec.add_dependency 'php_http_build_query', '~> 0.1'
end
