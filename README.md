# SplynxApi

A simple helper for [Splynx API](http://docs.splynx.apiary.io/) based on [REST Client](https://github.com/rest-client/rest-client).

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'splynx_api'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install splynx_api

## Usage
Get your API data from <Your splynx url>/admin/administration/api-keys.

Before use the API please check your API permissions.

```ruby
require 'splynx_api'

api = SplynxApi::Api.new({
  :base_url => '<protocol>://<domain>',
  :key => 'Your Splynx Api key',
  :secret => 'Your Splynx Api secret',
  :verify_ssl => false
})

# Get list of all locations
response = api.get('/admin/administration/locations')

puts response.code
puts response.body
```

## SplynxApi::Api reference

### new(params)
* :base_url - Splynx url with protocol
* :key - Splynx Api key
* :secret - Splynx Api secret
* :verify_ssl - Enable verify ssl (Default: true)

Create new instance of Api

### get(uri, id)
* uri - Required
* id - Optional

Get list of models. Optionally, an id may be provided to retrieve one a model.

Returned RestClient::Response or error object.

### post(uri, params)
* uri - Required
* params - Required

Create new model with provided attributes value.
Returned RestClient::Response or error object.

Example:
```ruby
# Create new location named "My Location"
response = api.post('/admin/administration/locations', {
  'name' => 'My Location'
})
```

### put(uri, id, params)
* uri - Required
* id - Required
* params - Required

Update existing model.

Returned RestClient::Response or error object.

Example:
```ruby
# Rename location with id 1 to "Updated Location"
response = api.put('/admin/administration/locations', 1, {
  'name' => 'My Location'
})
```

### search(uri, params)
* uri - Required
* params - Required

Search model by custom parameters.

Returned RestClient::Response or error object.

Example:
```ruby
# Search location with name "Updated Location"
response = api.search('/admin/administration/locations', {
  'main_attributes' => {
    'name' => 'Updated Location'
  }
})
```

### delete(uri, id)
* uri - Required
* id - Required

Delete model by id.

Returned RestClient::Response or error object.

Example:
```ruby
# Delete location with id 1
response = api.delete('/admin/administration/locations', 1)
```

### How to get administrator information
Set session hash property for get administrator information in next request.

Example:
```ruby
api.sash = 'Your session hash'
api.get('/admin/customers/customer')

puts api.administrator

# Returned:
#{
#  :id => 'Admin ID',
#  :role => 'Admin role',
#  :partner => 'Admin partner ID'
#}
``` 

## Development

After checking out the repo, run `bin/setup` to install dependencies.

To install this gem onto your local machine, run `bundle exec rake install`.

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/splynx/splynx-ruby-api.
