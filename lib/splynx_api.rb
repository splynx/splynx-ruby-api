require 'splynx_api/version'
require 'openssl'
require 'cgi'
require 'rest-client'
require 'php_http_build_query'

module SplynxApi
  class Api
    attr_accessor :sash

    def initialize(params)
      @base_url = params[:base_url]
      @key = params[:key]
      @secret = params[:secret]
      @verify_ssl = params[:verify_ssl].nil? ? true : params[:verify_ssl]
      @version = '1.0'
      @nonce_v = Time.now.to_i
      @sash = nil
      @administrator = nil
    end

    def administrator
      @administrator
    end

    def signature
      str = "#{@nonce_v}#{@key}"
      digest = OpenSSL::Digest::SHA256.new
      OpenSSL::HMAC.hexdigest(digest, @secret, str).upcase
    end

    def auth
      auth = {
          :nonce => @nonce_v,
          :key => @key,
          :signature => signature
      }
      unless @sash.nil?
        auth[:sash] = @sash
      end

      @nonce_v = @nonce_v + 1

      PHP.http_build_query(auth)
    end

    def url(uri, id = nil, params = nil)
      result = "#{@base_url}/api/#{@version}#{uri}"

      unless id.nil?
        result = "#{result}/#{id}"
      end
      unless params.nil?
        params_string = PHP.http_build_query(params)
        result = "#{result}?#{params_string}"
      end

      result
    end

    def process_response(response)
      headers = response.headers
      unless headers[:spl_administrator_id].nil?
        @administrator = {
            :id => headers[:spl_administrator_id],
            :role => headers[:spl_administrator_role],
            :partner => headers[:spl_administrator_partner]
        }
      end
      response
    end

    def perform(method, url, params = nil)
      begin
        response = RestClient::Request.execute(
            method: method,
            url: url,
            verify_ssl: @verify_ssl,
            payload: params,
            headers: {
                :Authorization => "Splynx-EA (#{auth})",
                :user_agent => "Splynx Ruby API #{@version}"
            })
        process_response(response)
      rescue StandardError => e
        e
      end
    end

    def get(uri, id = nil)
      perform(:get, url(uri, id))
    end

    def post(uri, params)
      perform(:post, url(uri), params)
    end

    def delete(uri, id)
      perform(:delete, url(uri, id))
    end

    def put(uri, id, params)
      perform(:put, url(uri, id), params)
    end

    def search(uri, params)
      perform(:get, url(uri, nil, params))
    end

    protected :signature, :auth, :url, :perform, :process_response
  end
end
